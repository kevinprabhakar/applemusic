package applemusic

type AppleMusicLibraryPlaylistCreationRequest struct{
	Attributes 				AppleMusicLibraryPlaylistCreationRequestAttributes 		`json:"attributes"`
	Relationships 			AppleMusicLibraryPlaylistCreationRequestRelationships	`json:"relationships,omitempty"`
}

type AppleMusicLibraryPlaylistCreationRequestAttributes struct{
	Description 			string 			`json:"description,omitempty"`
	Name 					string 			`json:"name"`
}

type AppleMusicLibraryPlaylistCreationRequestRelationships struct{
	Tracks 					[]AppleMusicLibraryPlaylistRequestTrack 	`json:"tracks,omitempty"`
}

type AppleMusicLibraryPlaylistRequestTrack struct{
	Id 						string 			`json:"id"`
	Type 					string 			`json:"type"`
}
