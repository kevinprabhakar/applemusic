package applemusic

import (
	"context"
	"fmt"
	"github.com/minchao/go-apple-music"
)

// SearchResults represents a results, that contains a map of search results.
// The members of the results object are the types of resources and the value for each is a Response Root object.
type SearchResults struct {
	Activities    *Activities    `json:"activities,omitempty"`
	Albums        *Albums        `json:"albums,omitempty"`
	AppleCurators *AppleCurators `json:"apple-curators,omitempty"`
	Artists       *Artists       `json:"artists,omitempty"`
	Curators      *Curators      `json:"curators,omitempty"`
	MusicVideos   *MusicVideos   `json:"music-videos,omitempty"`
	Playlists     *Playlists     `json:"playlists,omitempty"`
	Stations      *Stations      `json:"stations,omitempty"`
	Songs         *Songs         `json:"songs,omitempty"`
}

// Search represents the result of search for resources.
type Search struct {
	Results SearchResults `json:"results"`
}

// SearchOptions specifies the parameters to search the catalog.
type SearchOptions struct {
	// The entered text for the search with ‘+’ characters between each word,
	// to replace spaces (for example term=james+br).
	Term string `url:"term"`

	// (Optional) The localization to use, specified by a language tag.
	// The possible values are in the supportedLanguageTags array belonging to the Storefront object specified by storefront.
	// Otherwise, the storefront’s defaultLanguageTag is used.
	Language string `url:"l,omitempty"`

	// (Optional) The limit on the number of objects, or number of objects in the specified relationship, that are returned.
	// The default value is 5 and the maximum value is 25.
	Limit int `url:"limit,omitempty"`

	// (Optional; valid only with types specified) The next page or group of objects to fetch.
	Offset int `url:"offset,omitempty"`

	// (Optional) The list of the types of resources to include in the results.
	Types string `url:"types,omitempty"`
}

// Search searches the catalog using a query.
func (s *CatalogService) Search(ctx context.Context, storefront string, opt *SearchOptions) (*Search, *Response, error) {
	u := fmt.Sprintf("v1/catalog/%s/search", storefront)
	u, err := addOptions(u, opt)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	search := &Search{}
	resp, err := s.client.Do(ctx, req, search)
	if err != nil {
		return nil, resp, err
	}

	return search, resp, nil
}

// SearchHintsOptions specifies the parameters to search hints.
type SearchHintsOptions struct {
	Term     string `url:"term"`
	Language string `url:"l,omitempty"`
	Limit    int    `url:"limit,omitempty"` // (Optional) The number of search terms to be returned. The default value is 10.
	Types    string `url:"types,omitempty"`
}

// SearchHintsResults represents a results, that contains terms array.
type SearchHintsResults struct {
	Terms []string `json:"terms"`
}

// SearchHints represents the result of search hints.
type SearchHints struct {
	Results SearchHintsResults `json:"results"`
}

// SearchHints fetches the search term results for a hint.
func (s *CatalogService) SearchHints(ctx context.Context, storefront string, opt *SearchHintsOptions) (*SearchHints, *Response, error) {
	u := fmt.Sprintf("v1/catalog/%s/search/hints", storefront)
	u, err := addOptions(u, opt)
	if err != nil {
		return nil, nil, err
	}

	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	searchHints := &SearchHints{}
	resp, err := s.client.Do(ctx, req, searchHints)
	if err != nil {
		return nil, resp, err
	}

	return searchHints, resp, nil
}

type AppleMusicSearchQueryParams struct{
	SearchQuery 			string 			`json:"searchQuery"`
	Limit 					int 			`json:"limit"`
	Types 					[]string 		`json:"types"`
}

//Search functionality
//Had to create my own structs for the apple music search functionality because the github repo is outdated

//This one is just a test
type AppleMusicSearchOptions struct {
	// The entered text for the search with ‘+’ characters between each word,
	// to replace spaces (for example term=james+br).
	Term string `url:"term"`

	// (Optional) The localization to use, specified by a language tag.
	// The possible values are in the supportedLanguageTags array belonging to the Storefront object specified by storefront.
	// Otherwise, the storefront’s defaultLanguageTag is used.
	Language string `url:"l,omitempty"`

	// (Optional) The limit on the number of objects, or number of objects in the specified relationship, that are returned.
	// The default value is 5 and the maximum value is 25.
	Limit int `url:"limit,omitempty"`

	// (Optional; valid only with types specified) The next page or group of objects to fetch.
	Offset int `url:"offset,omitempty"`

	// (Optional) The list of the types of resources to include in the results.
	Types string `url:"types,omitempty"`
}


type AppleMusicArtists struct {
	Data []AppleMusicArtist `json:"data"`
	Href string   `json:"href,omitempty"`
	Next string   `json:"next,omitempty"`
}

type AppleMusicArtistAttributes struct {
	GenreNames     []string        `json:"genreNames"`
	EditorialNotes *applemusic.EditorialNotes `json:"editorialNotes,omitempty"`
	Name           string          `json:"name"`
	URL            string          `json:"url"`
}

type AppleMusicArtistRelationships struct {
	Albums      applemusic.Albums       `json:"albums"`
	Genres      *applemusic.Genres      `json:"genres,omitempty"`
	MusicVideos *applemusic.MusicVideos `json:"music-videos,omitempty"`
	Playlists   *applemusic.Playlists   `json:"playlists,omitempty"`
}

type AppleMusicArtist struct {
	Id            string              `json:"id"`
	Type          string              `json:"type"`
	Href          string              `json:"href"`
	Attributes    AppleMusicArtistAttributes    `json:"attributes"`
	Relationships AppleMusicArtistRelationships `json:"relationships"`
}

type AppleMusicSongAttributes struct {
	Artwork          applemusic.Artwork         `json:"artwork"`
	ArtistName       string          `json:"artistName"`

	//Album name is currently the only field not added in the apple music github repo
	//Should probably file a github issue, but this is our hacky fix for now
	AlbumName 		 string 		 `json:"albumName"`
	URL              string          `json:"url"`
	DiscNumber       int             `json:"discNumber"`
	GenreNames       []string        `json:"genreNames"`
	ISRC             string          `json:"isrc"`
	DurationInMillis int64           `json:"durationInMillis,omitempty"`
	ReleaseDate      string          `json:"releaseDate"`
	Name             string          `json:"name"`
	PlayParams       *applemusic.PlayParameters `json:"playParams,omitempty"`
	TrackNumber      int             `json:"trackNumber,omitempty"`
	ComposerName     string          `json:"composerName,omitempty"`
	ContentRating    string          `json:"contentRating,omitempty"`
	EditorialNotes   *applemusic.EditorialNotes `json:"editorialNotes,omitempty"`
	MovementCount    int             `json:"movementCount,omitempty"`
	MovementName     string          `json:"movementName,omitempty"`
	MovementNumber   int             `json:"movementNumber,omitempty"`
	WorkName         string          `json:"workName,omitempty"`
}

type AppleMusicSongRelationships struct{
	Albums  applemusic.Albums  `json:"albums"`           // Default inclusion: Identifiers only
	Artists applemusic.Artists `json:"artists"`          // Default inclusion: Identifiers only
	Genres  *applemusic.Genres `json:"genres,omitempty"` // Default inclusion: None
}

type AppleMusicSong struct {
	Id            string            `json:"id"`
	Type          string            `json:"type"`
	Href          string            `json:"href"`
	Attributes    AppleMusicSongAttributes    `json:"attributes"`
	Relationships AppleMusicSongRelationships `json:"relationships"`
}

type AppleMusicSongs struct {
	Data []AppleMusicSong `json:"data"`
	Href string   `json:"href,omitempty"`
	Next string   `json:"next,omitempty"`
}

type AppleMusicSearchResultsAbstraction struct {
	Artists       *AppleMusicArtists       `json:"artists,omitempty"`
	Songs         *AppleMusicSongs         `json:"songs,omitempty"`
}

type AppleMusicSearchResults struct{
	Results AppleMusicSearchResultsAbstraction `json:"results"`
}
