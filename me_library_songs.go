package applemusic

type LibrarySongs struct{
	Data 			[]LibrarySong 		`json:"data"`
}

type LibrarySong struct{
	Attributes 		LibrarySongAttributes 		`json:"attributes"`
	Relationships 	LibrarySongRelationships	`json:"relationships"`
	Type 			string 						`json:"type"`
	Href 			string 						`json:"href"`
	Id 				string 						`json:"id"`
}

type LibrarySongAttributes struct{
	AlbumName 		string 						`json:"albumName"`
	ArtistName 		string 						`json:"artistName"`
	Artwork     	Artwork 					`json:"artwork"`
	ContentRating   string 						`json:"contentRating"`
	DiscNumber 		int 						`json:"discNumber"`
	Milliseconds	int 						`json:"durationInMilliseconds"`
	Name 			string 						`json:"name"`
	PlayParameters  LibraryPlayParameters 	`json:"playParams"`
	TrackNumber 	int 						`json:"trackNumber"`
}

type LibraryPlayParameters struct{
	Id 				string 						`json:"id"`
	Kind 			string 						`json:"kind"`
}

type LibrarySongRelationships struct{
	Albums 			LibraryAlbumRelationship	`json:"albums"`
	Artists 		LibraryArtistRelationship	`json:"artists"`
}

type LibraryAlbumRelationship struct{
	Data 			[]LibraryAlbum 				`json:"data"`
}

type LibraryAlbum struct{
	Attributes  	LibraryAlbumAttributes 		`json:"attributes"`
	Relationships 	LibraryAlbumRelationship 	`json:"relationships"`
	Type 			string 						`json:"type"`
}

type LibraryAlbumAttributes struct{
	ArtistName 		string 						`json:"artistName"`
	Artwork     	Artwork 					`json:"artwork"`
	ContentRating   string 						`json:"contentRating"`
	Name 			string 						`json:"name"`
	PlayParameters  LibraryPlayParameters 				`json:"playParams"`
	TrackCount 		int 						`json:"trackCount"`
}

type LibraryArtistRelationship struct {
	Data 			[]LibraryArtist 			`json:"data"`
}

type LibraryArtist struct {
	Attributes 		LibraryArtistAttributes 	`json:"attributes"`
	Relationships 	LibraryArtistRelationships	`json:"relationships"`
	Type 			string 						`json:"type"`
}

type LibraryArtistAttributes struct {
	Name 			string 						`json:"name"`
}

type LibraryArtistRelationships struct{
	Albums 			LibraryAlbumRelationship 	`json:"albums"`
}




