package applemusic

import "github.com/minchao/go-apple-music"

//Recommendations functionality
type AppleMusicRecommendationResponse struct{
	Data 		[]AppleMusicRecommendation		`json:"data"`
}

type AppleMusicRecommendation struct{
	Id 				string 									`json:"id"`
	Type 			string 									`json:"type"`
	Href 			string 									`json:"href"`
	Attributes 		AppleMusicRecommendationAttributes		`json:"attributes"`
	Relationships	AppleMusicRecommendationRelationships	`json:"relationships"`
}

type AppleMusicRecommendationAttributes struct{
	IsGroupRecommendation	bool 			`json:"isGroupRecommendation"`
	NextUpdateDate 			string 			`json:"nextUpdateDate"`
	Reason					string 			`json:"reason,omitempty"`
	ResourceTypes 			[]string 		`json:"resourceTypes"`
	Title 					AppleMusicRecommendationAttributesTitle 			`json:"title,omitempty"`
}

type AppleMusicRecommendationAttributesTitle struct{
	StringForDisplay 		string 			`json:"stringForDisplay"`
	ContentIds 				[]string 		`json:"contentIds"`
}

type AppleMusicRecommendationRelationships struct{
	Contents 				AppleMusicRecommendationRelationship 	`json:"content"`
	Recommendations 		AppleMusicRecommendationRelationship 	`json:"recommendations"`
}

type AppleMusicRecommendationRelationship struct{
	Data 					[]applemusic.Resource 					`json:"data"`
	Href 					string 									`json:"href"`
	Next 					string 									`json:"next"`
}
